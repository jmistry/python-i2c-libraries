Python libraries for I2C chips on the Raspberry Pi
====================

This is a collection of Python 2.7 Raspberry Pi libraries for interfacing with various ICs connected to the Pi's I2C bus.

Supported ICs:

IC Name       | Description
------------- | -------------
TLC59116      | Texas Instruments 16-channel Fm+ I2C-bus constant-current LED sink driver
TMP175        | Texas Instruments digital temperature sensor with I2C interface