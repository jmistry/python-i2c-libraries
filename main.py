#!/usr/bin/python
import time
import math
from TLC59116 import TLC59116
from TMP175 import TMP175

# I2C device addresses
LED_DRIVER_ADDR = 0x60
TEMP_SENSOR_ADDR = 0x48

# Class instances
LED = TLC59116(LED_DRIVER_ADDR)
Temp = TMP175(TEMP_SENSOR_ADDR)

# Main loop
while True:
	# LED.demo()
	print(Temp.read_temp())
	
	break
	