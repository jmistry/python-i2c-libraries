#!/usr/bin/env python3

import sys
# appends to PYTHONPATH the location of the example codes
sys.path.append(r'/home/pi/git/quick2wire-python-api/')

import quick2wire.i2c as i2c
import time
import math
from array import *

# Here the address that the port expander is at is set
PORT_EXPANDER_ADDR = 0x20
LED_DRIVER_ADDR = 0x60
TEMP_SENSOR_ADDR = 0x48

# Globals
temp_high = 0.0
temp_low = 100.0
long_avg = array('f',[])
short_avg = array('f',[0, 0, 0, 0, 0])
long_avg_index = 0
short_avg_index = 0

# bus.transaction(i2c.writing_bytes(address, reg, data))

def testPortExpander():
	# Port Expander things
	with i2c.I2CMaster() as bus:
		bus.transaction(i2c.writing_bytes(PORT_EXPANDER_ADDR,  0x00, 0x00))	# Set Port A to output
		bus.transaction(i2c.writing_bytes(PORT_EXPANDER_ADDR,  0x12, 0xFF))	# Turn on all of Port A
		time.sleep(1)
		bus.transaction(i2c.writing_bytes(PORT_EXPANDER_ADDR,  0x12, 0x00))	# Turn off all of Port A
		time.sleep(1)
		
def testLEDdriver():
	# LED Driver things
	with i2c.I2CMaster() as bus:
		bus.transaction(i2c.writing_bytes(LED_DRIVER_ADDR,  0x00, 0b00000000))	# Set Oscillator On
		bus.transaction(i2c.writing_bytes(LED_DRIVER_ADDR,  0x14, 0b11111111))  # Set PWM0 to output
		
		bus.transaction(i2c.writing_bytes(LED_DRIVER_ADDR,  0x02, 0b11111111))	# Set PWM0 to full brightness
		time.sleep(1)
		bus.transaction(i2c.writing_bytes(LED_DRIVER_ADDR,  0x02, 0b00000000))	# Set PWM0 to zero brightness
		time.sleep(1)
	
def readTemp():
	global temp_low
	global temp_high
	
	# Temp Sensor
	with i2c.I2CMaster() as bus:
		bus.transaction(i2c.writing_bytes(TEMP_SENSOR_ADDR, 0b00000001, 0b01100000))
		bus.transaction(i2c.writing_bytes(TEMP_SENSOR_ADDR, 0b00000000))
		tempHiBits, tempLoBits = bus.transaction(i2c.reading(TEMP_SENSOR_ADDR, 2))[0]
		#print(tempHiBits, bin(tempLoBits))
		fullTemp = (tempHiBits + (tempLoBits/256))
		#print("fullTemp: ", fullTemp)
		if (fullTemp < temp_low):
			temp_low = fullTemp
		if (fullTemp > temp_high):
			temp_high = fullTemp		
		return(fullTemp)
		
def readScaledTemp():
	global temp_low
	global temp_high
	
	unscaledTemp = readTemp()
	
	scale_max = 100
	scale_min = 0
	
	# scaling eqn.: y = mx + c
	m = (scale_max - scale_min)/(temp_high - temp_low + 0.001)
	c = scale_min - m*temp_low
	
	scaledTemp = m*unscaledTemp + c
	
	print("Unscaled Temp: ", "%.4f" % unscaledTemp, " y = ", "%.4f" % m, "x + ", "%.4f" % c, " Scaled Temp: ", "%.2f" % scaledTemp, "t_hi: %.4f" % temp_high, "t_lo: %.4f" % temp_low)
	
	return(scaledTemp)
	
def readScaledTemp2():
	global long_avg
	global short_avg
	global long_avg_index
	global short_avg_index
	
	unscaledTemp = readTemp()
	
	short_avg.pop()
	short_avg.insert(0, unscaledTemp)
	
	printStr = ""
	
	for i in range (0,4):
		printStr += "%.4f" % short_avg[i]
		printStr += " "
	
	print(printStr)
	
	
	
	
		
# The main parts of the program start here
while True:
	#readScaledTemp2()
	#time.sleep(0.1)
	#testPortExpander()

	testLEDdriver()
