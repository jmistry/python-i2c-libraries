#!/usr/bin/python
# Filename: TMP175.py

import smbus

class TMP175:
	""" Library for the TMP175 I2C digital temperature sensor. """
	
	# Set-up the I2C bus
	bus = smbus.SMBus(1)

	# I2C device address
	__TEMP_SENSOR_ADDR = None
	
	def __init__(self, address=0x48):
		""" Initialises temp sensor. 
		
		Configuration register at 0b01:
			D7: OS	- One-Shot mode (one temperature conversion)
			D6: R1	- Converter resolution 1
			D5: R0	- Converter resolution 0
			D4: F1	- Fault Queue 1
			D3: F0	- Fault Queue 0
			D2: POL	- Polarity of Alert pin (POL = 0, active LOW)
			D1: TM	- Thermostat Mode (can generate interrupts on Alert pin)
			D0: SD	- Shutdown Mode (power saving)
		"""
		
		# Save address
		self.TEMP_SENSOR_ADDR = address
		
		# Device configuration
		configuration = 0b01100001 # Highest resolution, shutdown mode on
		self.bus.write_word_data(self.TEMP_SENSOR_ADDR, 0b01, configuration) # Write config to configuration register
		
	def read_temp(self):
		""" Returns the current temperature. """
		
		# Start one shot mode
		old_config = self.bus.read_byte_data(self.TEMP_SENSOR_ADDR, 0b01)
		new_config = old_config & 0b10000000
		self.bus.write_word_data(self.TEMP_SENSOR_ADDR, 0b01, new_config) # Write new_config to configuration register
		
		# Read temperature
		temp_read = self.bus.read_word_data(self.TEMP_SENSOR_ADDR, 0b00)
		temp_lo = (temp_read & 0xFF00) >> 8
		temp_hi = (temp_read & 0x00FF)
		full_temp = (((temp_hi * 256) + temp_lo) >> 4) * 0.0625 
				
		return(full_temp)
		
def main():
	''' Print the current temp. '''
	
	print("Current Temperature = " + str(TMP175().read_temp()))

if __name__ == "__main__":
	main()
	