#!/usr/bin/python
# Filename: TLC59116.py

import time
import smbus

class TLC59116:
	""" Library for the TLC59116 I2C 16 channel PWM LED driver. """
	
	# Set-up the I2C bus
	bus = smbus.SMBus(1)

	# I2C device address
	__LED_DRIVER_ADDR = None

	# LED Registers
	PWM = [0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11]

	def __init__(self, address=0x60):
		""" Initialises LED driver. """
		self.LED_DRIVER_ADDR = address
		self.bus.write_byte_data(self.LED_DRIVER_ADDR, 0x00, 0b00000000)  # Set Mode, Oscillator on
		self.bus.write_byte_data(self.LED_DRIVER_ADDR, 0x14, 0b11111111)  # Set LED0-3   to PWM output
		self.bus.write_byte_data(self.LED_DRIVER_ADDR, 0x15, 0b11111111)  # Set LED4-7   to PWM output
		self.bus.write_byte_data(self.LED_DRIVER_ADDR, 0x16, 0b11111111)  # Set LED8-11  to PWM output
		self.bus.write_byte_data(self.LED_DRIVER_ADDR, 0x17, 0b11111111)  # Set LED12-15 to PWM output
		
	def demo(self):
		""" Turns on and off each all LEDs. """
		# On
		for LED in self.PWM:
			self.bus.write_byte_data(self.LED_DRIVER_ADDR, LED, 255)
			time.sleep(0.1)
			
		# Off
		for LED in self.PWM:
			self.bus.write_byte_data(self.LED_DRIVER_ADDR, LED, 0)
			time.sleep(0.1)
			
	def set_led(self, LED, brightness):
		""" Sets the brightness (0 to 255) of an LED """
		# Input scrubbing
		if brightness < 0:
			brightness = 0
		elif brightness > 255:
			brightness = 255
		
		# Write to LED
		self.bus.write_byte_data(self.LED_DRIVER_ADDR, LED, brightness)
		
	def set_all_leds(self, brightness):
		""" Sets the brightness (0 to 255) of an LED """
		# Input scrubbing
		if brightness < 0:
			brightness = 0
		elif brightness > 255:
			brightness = 255
		
		# Write to LEDS
		self.bus.write_byte_data(self.LED_DRIVER_ADDR, 0x12, brightness)
		
def main():
	''' Run demo. '''
	
	print("Demo running... ")
	TLC59116().demo()
	print("Done!")

if __name__ == "__main__":
	main()
	